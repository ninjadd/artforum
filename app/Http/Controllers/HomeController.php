<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        
        if (Cache::has('artworks/261620')) {
            $status = 'from cache';
            $response = Cache::get('artworks/261620');            
        } else {
            $status = 'from api';
            $response = Http::get('https://api.artic.edu/api/v1/artworks/261620');

            Cache::put('artworks/261620', $response->json(), now()->addDays(90));

            $response->json();
        }

        return [$status, $response];


        return view('home');
    }
}
