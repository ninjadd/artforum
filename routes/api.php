<?php

use App\Http\Controllers\TokenController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('tokens/user', [TokenController::class, 'store']);

Route::middleware('auth:sanctum')->group(function () { 
    
    Route::controller(UserController::class)->group(function () {
        Route::get('user', 'show');
    });

    Route::controller(TokenController::class)->group(function () {

        Route::post('tokens', 'create');

    });

});
